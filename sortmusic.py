# |/usr/bin/env python3

"Sort a list of songs based on their number of plays."

import sys

songs = {}

# No utilizo el i para nada, no lo incluyo, lo dejo por el test
def order_items(songs: list, i: int, gap: int) -> list:
    n = len(songs)

    for i in range(gap, n):
        temp_title, temp_reproductions = songs[i]
        j = i
        while j >= gap and songs[j-gap][1] < temp_reproductions:
            songs[j] = songs[j - gap]
            j -= gap
            songs[j] = (temp_title, temp_reproductions)

    return songs

def sort_music(songs: list) -> list:
    n = len(songs)
    gap = n // 2

    while gap > 0:
        for i in range(gap, n):
            songs = order_items(songs, i, gap)
        gap //= 2

    return songs


def create_dictionary(arguments: list) -> dict:
    # creamos el diccionario
    songs: dict = {}
    i = 0
    while i < len(arguments):
        song_name = arguments[i]
        i += 1
        if i < len(arguments):
            try:
                reproductions = int(arguments[i])
                songs[song_name] = reproductions
            except ValueError:
                sys.exit(f"Error: El argumento '{arguments[i]}' no es un número de reproducciones válido.")
        else:
            sys.exit("Error: Debes proporcionar un número de reproducciones para cada canción.")
        i += 1
    return songs


def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0 or len(args) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs: dict = create_dictionary(args)

    sorted_songs: list = sort_music(list(songs.items()))

    sorted_dict: dict = dict(sorted_songs)
    print(sorted_dict)


if __name__ == '__main__':
    main()